package com.example.homework1_giorgimatcharashvili

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var answer: String = ""

        fun generateRandomNumber(): Int {
            return (-100..100).random()
        }
        fun checkNumber(): String {
            val randomNumber = generateRandomNumber()
            answer = if(randomNumber/5 > 0 && randomNumber%5==0){
                "Yes"
            } else "No"
            return answer
        }
        randomNumberGeneratorButton.setOnClickListener{
            val answer= checkNumber()
            randomNumberOutputTextView.text = answer
        }
    }
}